import './Form.scss';
import {FormEvent, ReactNode} from 'react';

interface FormProps {
    children: string | ReactNode | ReactNode[]
    onSubmit: () => void
}

const Form = (props: FormProps) => {
    const onSubmit = (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        props.onSubmit();
    }

    return (
        <form className='form' onSubmit={onSubmit}>
            {props.children}
        </form>
    );
}

export default Form;
