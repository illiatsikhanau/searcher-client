import './Input.scss';
import React, {ChangeEvent} from 'react';
import InputMask from 'react-input-mask';

interface InputProps {
    value: string
    setValue: (value: string) => void
    name: string
    required?: boolean
    mask?: string | (string | RegExp)[]
    autoFocus?: boolean
}

const Input = (props: InputProps) => {
    const onChange = (e: ChangeEvent<HTMLInputElement>) => {
        props.setValue(e.target.value);
    }

    return (
        <div className='input'>
            <p className='input-name'>
                {props.name}
                {props.required && <span className='input-required-mark'>*</span>}
            </p>
            <InputMask
                className='input-field'
                value={props.value}
                onChange={onChange}
                mask={props.mask ?? ''}
                autoFocus={props.autoFocus}
            />
        </div>
    );
}

export default Input;
