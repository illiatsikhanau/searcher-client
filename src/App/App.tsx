import './App.scss';
import React, {useState} from 'react';
import axios from 'axios';
import Form from '../Form/Form';
import Input from '../Input/Input';
import Button from '../Button/Button';
import Records from '../Records/Records';

const App = () => {
    const [email, setEmail] = useState('');
    const [number, setNumber] = useState('');
    const [error, setError] = useState('');
    const [controller, setController] = useState(new AbortController());
    const [records, setRecords] = useState([]);

    const onSubmit = () => {
        if (!isEmailValid(email)) {
            setError('Email is invalid!');
        } else if (!isNumberValid(number)) {
            setError('Number is invalid!');
        } else {
            setError('');
            setRecords([]);

            controller.abort();
            const newController = new AbortController();
            setController(newController);

            axios.get('/api/data', {
                signal: newController.signal,
                params: {
                    email: email,
                    number: number.replaceAll('-', '')
                }
            }).then(res => {
                setRecords(res.data);
            }).catch(() => {
            });
        }
    }

    const isEmailValid = (email: string) => {
        return email.includes('@');
    }

    const isNumberValid = (number: string) => {
        return !number.includes('_');
    }

    return (
        <main className='app'>
            <Form onSubmit={onSubmit}>
                <Input name='Email' value={email} setValue={setEmail} required autoFocus/>
                <Input name='Number' value={number} setValue={setNumber} mask='99-99-99'/>
                {error && <p className='form-error'>{error}</p>}
                <Button>
                    Submit
                </Button>
            </Form>
            {records.length ? <Records records={records}/> : ''}
        </main>
    );
}

export default App;
