import './Button.scss';
import {ReactNode} from 'react';

interface ButtonProps {
    children: string | ReactNode | ReactNode[]
}

const Button = (props: ButtonProps) => {
    return (
        <button className='button'>
            {props.children}
        </button>
    );
}

export default Button;
