import './Records.scss';
import React from 'react';
import Record from './Record';

interface RecordsProps {
    records: Record[]
}

const Records = (props: RecordsProps) => {
    return (
        <div className='records'>
            {props.records.map((record, index) =>
                <div key={index} className='record'>
                    <p className='record-field'>
                        <span className='record-field-label'>Email:</span>
                        {record.email}
                    </p>
                    <p className='record-field'>
                        <span className='record-field-label'>Number:</span>
                        {record.number}
                    </p>
                </div>
            )}
        </div>
    )
}

export default Records;
